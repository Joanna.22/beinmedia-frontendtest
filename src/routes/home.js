import React from 'react';
import VideoBox from "../components/videoBox";
import { useMediaQuery } from 'react-responsive';

function Home () {
    const isMobile = useMediaQuery({
        query: '(max-device-width: 991px)'
    })
    return(
        <div className={isMobile ? 'main-container mob' : 'main-container'}>
            <div className="row mob-rev">
                <div className="col-lg-4">
                    <div className="text-box">
                        <div className="text-box-container">
                            <p className="text">
                                هند الناهض مستشارة إعلام رقمي, مستشارة الإعلام الإلكتروني لمكتب وكيل وزارة الإعلام الكويتية وهي المؤسسة لشركة “سوشالوبي” للخدمات المختصة في مجال الإعلام الإجتماعي , بالإضافة الى إنها مستشارة في مجال التواصل الاجتماعي.هند الناهض مستشارة إعلام رقمي, مستشارة الإعلام الإلكتروني لمكتب وكيل وزارة الإعلام الكويتية وهي المؤسسة لشركة “سوشالوبي” للخدمات المختصة في مجال الإعلام الإجتماعي , بالإضافة الى إنها مستشارة في مجال التواصل الاجتماعي.
                                <br></br>
                                هند الناهض مستشارة إعلام رقمي, مستشارة الإعلام الإلكتروني لمكتب وكيل وزارة الإعلام الكويتية وهي المؤسسة لشركة “سوشالوبي” للخدمات المختصة في مجال الإعلام الإجتماعي , بالإضافة الى إنها مستشارة في مجال التواصل الاجتماعي.هند الناهض مستشارة إعلام رقمي, مستشارة الإعلام الإلكتروني لمكتب وكيل وزارة الإعلام الكويتية وهي المؤسسة لشركة “سوشالوبي” للخدمات المختصة في مجال الإعلام الإجتماعي , بالإضافة الى إنها مستشارة في مجال التواصل الاجتماعي.
                            </p>
                        </div>
                    </div>
                    {isMobile? 
                        <div className="skills-box">
                            <p className="skills-title">الخبرات</p>           
                            <div className="row skills-grid">             
                                <span className="skill col-6">تطوير الأعمال</span>
                                <span className="skill col-6">المشاريع الصغيرة</span>
                                <span className="skill col-6">القيادة</span>
                                <span className="skill col-6">التحدث أمام الجمهور</span>
                                <span className="skill col-6">الدعاية والإعلان</span>
                                <span className="skill col-6">الإعلام</span>
                                <span className="skill col-6">التسويق الرقمي</span>
                                <span className="skill col-6">مشاركة العملاء</span>
                                
                            </div>
                        </div>
                    : 
                        <div className="skills-box">
                            <p className="skills-title">الخبرات</p>                        
                            <span className="skill">تطوير الأعمال</span>
                            <span className="skill">المشاريع الصغيرة</span>
                            <span className="skill">مشاركة العملاء</span>
                            <span className="skill">الإعلام</span>
                            <span className="skill">التحدث أمام الجمهور</span>
                            <span className="skill">الدعاية والإعلان</span>
                            <span className="skill">ريادة الأعمال</span>
                            <span className="skill">القيادة</span>
                            <span className="skill">التسويق الرقمي</span>
                        </div>
                    }
                </div>
                <div className="col-lg-8">
                    <VideoBox img="video" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" type="video" file="word"/>
                </div>
            </div>
        </div>
    )
}

export default Home;