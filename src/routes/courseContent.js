import React, {useState} from 'react';
import SmallVideo from "../components/smallVideo";
import VideoBox from "../components/videoBox";
import { useMediaQuery } from 'react-responsive';
import {useParams} from "react-router-dom";
import { Link } from 'react-router-dom';

function CourseContent () {
    let { id } = useParams();
    const isMobile = useMediaQuery({
        query: '(max-device-width: 991px)'
    })
    const courses = [
        {
            img:"course-1", 
            title:"تعرف على أسرار صناعة المحتوى التسويقي الفعال", 
            cost:"299", 
            num:"24"
        },
        {
            img:"course-1", 
            title:"تعرف على أسرار صناعة المحتوى التسويقي الفعال", 
            cost:"299", 
            num:"24"
        },
        {
            img:"course-2", 
            title:"تعرف على أسرار صناعة المحتوى التسويقي الفعال", 
            cost:"299", 
            num:"22"
        },
        {
            img:"course-3", 
            title:"تعرف على أسرار صناعة المحتوى التسويقي الفعال", 
            cost:"299", 
            num:"23"
        },
        {
            img:"course", 
            title:"تعرف على أسرار صناعة المحتوى التسويقي الفعال", 
            cost:"299", 
            num:"20"
        },
        {
            img:"course-5", 
            title:"تعرف على أسرار صناعة المحتوى التسويقي الفعال", 
            cost:"299", 
            num:"15"
        },
        {
            img:"course-6", 
            title:"تعرف على أسرار صناعة المحتوى التسويقي الفعال", 
            cost:"299", 
            num:"18"
        },
        {
            img:"course-7", 
            title:"تعرف على أسرار صناعة المحتوى التسويقي الفعال", 
            cost:"299", 
            num:"12"
        },
        {
            img:"course-8", 
            title:"تعرف على أسرار صناعة المحتوى التسويقي الفعال", 
            cost:"299", 
            num:"10"
        },
        {
            img:"course-9", 
            title:"تعرف على أسرار صناعة المحتوى التسويقي الفعال", 
            cost:"299", 
            num:"32"
        },
        {
            img:"course-10", 
            title:"تعرف على أسرار صناعة المحتوى التسويقي الفعال", 
            cost:"299", 
            num:"30"
        },
    ]
    return(
        <div className={isMobile ? 'main-container mob' : 'main-container'}>
            {isMobile ?
                <div className="cc">
                    <div className="text-box c">
                        <div className="text-box-container">
                            <p className="course-desc">وصف الكورس</p>
                            <p className="text">
                                لوريم إيبسوم هو ببساطة نص شكلي بمعنى أن الغاية هي الشكل وليس المحتوى ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، 
                            </p>
                        </div>
                    </div>
                    <VideoBox img={courses[id].img} title={courses[id].title} type="video"/>
                    <div className="row videos-row c">
                        <div className="col-md-4 col-6">
                            <Link to="/course/1">
                                <SmallVideo img="thumb-4" title="دورة صناعة المحتوى مع هند الناهض" cost="550" download="12" type="video"/>
                            </Link>
                        </div>
                        <div className="col-md-4 col-6">
                            <Link to="/course/2">
                                <SmallVideo img="thumb-5" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="149" download="39" type="video"/>
                            </Link>
                        </div>
                        <div className="col-md-4 col-6">
                            <Link to="/course/3">
                                <SmallVideo img="thumb-3" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="299" download="104" type="video"/>
                            </Link>
                        </div>
                        <div className="col-md-4 col-6">
                            <Link to="/course/4">
                                <SmallVideo img="thumb" title="دورة صناعة المحتوى مع هند الناهض" cost="550" download="12" type="video"/>
                            </Link>
                        </div>
                    </div>
                    <p className="course-sub-title">صناعة محتوي تسويقي فعال </p>
                    <div className="row videos-row c">
                        <div className="col-md-4 col-6">
                            <Link to="/course/5">
                                <SmallVideo img="thumb-2" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="149" download="39" type="video"/>
                            </Link>
                        </div>
                        <div className="col-md-4 col-6">
                            <Link to="/course/6">
                                <SmallVideo img="thumb-3" title="دورة صناعة المحتوى مع هند الناهض" cost="299" download="104" type="video"/>
                            </Link>
                        </div>
                    </div>
                </div>
            :
                <div className="row">
                        <div className="col-4">
                            <div className="text-box c">
                                <div className="text-box-container">
                                    <p className="course-desc">وصف الكورس</p>
                                    <p className="text">
                                        لوريم إيبسوم هو ببساطة نص شكلي بمعنى أن الغاية هي الشكل وليس المحتوى ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف.
                                    </p>
                                </div>
                            </div>
                            <div className="row videos-row c">
                                <div className="col-4">
                                    <Link to="/course/1">
                                        <SmallVideo img="thumb-4" title="دورة صناعة المحتوى مع هند الناهض" cost="550" download="12" type="video"/>
                                    </Link>
                                </div>
                                <div className="col-4">
                                    <Link to="/course/2">
                                        <SmallVideo img="thumb-5" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="149" download="39" type="video"/>
                                    </Link>
                                </div>
                                <div className="col-4">
                                    <Link to="/course/3">
                                        <SmallVideo img="thumb-3" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="299" download="104" type="video"/>
                                    </Link>
                                </div>
                                <div className="col-4">
                                    <Link to="/course/4">
                                        <SmallVideo img="thumb" title="دورة صناعة المحتوى مع هند الناهض" cost="550" download="12" type="video"/>
                                    </Link>
                                </div>
                                <div className="col-4">
                                    <Link to="/course/5">
                                        <SmallVideo img="thumb-2" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="149" download="39" type="video"/>
                                    </Link>
                                </div>
                                <div className="col-4">
                                    <Link to="/course/6">
                                        <SmallVideo img="thumb-3" title="دورة صناعة المحتوى مع هند الناهض" cost="299" download="104" type="video"/>
                                    </Link>
                                </div>
                            </div>
                            <p className="course-sub-title">صناعة محتوي تسويقي فعال </p>
                            <div className="row videos-row c">
                                <div className="col-4">
                                    <Link to="/course/1">
                                        <SmallVideo img="thumb-4" title="دورة صناعة المحتوى مع هند الناهض" cost="550" download="12" type="video"/>
                                    </Link>
                                </div>
                                <div className="col-4">
                                    <Link to="/course/2">
                                        <SmallVideo img="thumb-5" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="149" download="39" type="video"/>
                                    </Link>
                                </div>
                                <div className="col-4">
                                    <Link to="/course/3">
                                        <SmallVideo img="thumb-3" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="299" download="104" type="video"/>
                                    </Link>
                                </div>
                            </div>
                            <p className="course-sub-title">صناعة محتوي تسويقي فعال </p>
                            <div className="row videos-row c">
                                <div className="col-4">
                                    <Link to="/course/4">
                                        <SmallVideo img="thumb" title="دورة صناعة المحتوى مع هند الناهض" cost="550" download="12" type="video"/>
                                    </Link>
                                </div>
                                <div className="col-4">
                                    <Link to="/course/5">
                                        <SmallVideo img="thumb-2" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="149" download="39" type="video"/>
                                    </Link>
                                </div>
                                <div className="col-4">
                                    <Link to="/course/6">
                                        <SmallVideo img="thumb-3" title="دورة صناعة المحتوى مع هند الناهض" cost="299" download="104" type="video"/>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        <div className="col-8 course-big-vid-container">
                            <VideoBox img={courses[id].img} title={courses[id].title} type="video"/>
                        </div>
                    </div>  
            }
        </div>
    )
}

export default CourseContent;