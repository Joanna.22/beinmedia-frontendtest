import React from 'react';
import Course from "../components/course";
import { useMediaQuery } from 'react-responsive';
import { Link } from 'react-router-dom';

function Courses () {
    const isMobile = useMediaQuery({
        query: '(max-device-width: 991px)'
    })
    return(
        <div className={isMobile ? "main-courses-container mob" : "main-courses-container"}>
            <div className="row">
                <div className="col-lg-3">
                    <Link to="/course/1">
                        <Course img="course-1" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="299" num="24"/>
                    </Link>
                </div>
                <div className="col-lg-3">
                    <Link to="/course/2">
                        <Course img="course-2" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="299" num="22"/>
                    </Link>
                </div>
                <div className="col-lg-3">
                    <Link to="/course/3">
                        <Course img="course-3" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="299" num="23"/>
                    </Link>
                </div>
                <div className="col-lg-3">
                    <Link to="/course/4">
                        <Course img="course" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="299" num="20"/>
                    </Link>
                </div>
                <div className="col-lg-3">
                    <Link to="/course/5">
                        <Course img="course-5" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="299" num="15"/>
                    </Link>
                </div>
                <div className="col-lg-3">
                    <Link to="/course/6">
                        <Course img="course-6" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="299" num="18"/>
                    </Link>
                </div>
                <div className="col-lg-3">
                    <Link to="/course/7">
                        <Course img="course-7" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="299" num="12"/>
                    </Link>
                </div>
                <div className="col-lg-3">
                    <Link to="/course/8">
                        <Course img="course-8" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="299" num="10"/>
                    </Link>
                </div>
                <div className="col-lg-3">
                    <Link to="/course/9">
                        <Course img="course-9" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="299" num="32"/>
                    </Link>
                </div>
                <div className="col-lg-3">
                    <Link to="/course/10">
                        <Course img="course-10" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="299" num="30"/>
                    </Link>
                </div>
                <div className="col-lg-3">
                    
                </div>
                <div className="col-lg-3">
                    
                </div>
            </div>
        </div>
    )
}

export default Courses;