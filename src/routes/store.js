import React, {useRef, useState} from 'react';
import VideoBox from "../components/videoBox";
import SmallVideo from "../components/smallVideo";
import { useMediaQuery } from 'react-responsive';

function Store () {
    const isMobile = useMediaQuery({
        query: '(max-device-width: 991px)'
    })
    const [img,setImg] = useState("video");
    const [title,setTitle] = useState("تعرف على أسرار صناعة المحتوى التسويقي الفعال");
    const [type,setType] = useState("video");
    const [file,setFile] = useState("word");
    const showContent = (img,title,type,file) => {
        setImg(img);
        setTitle(title);
        setType(type)
        setFile(file)
    }
    return(
        <div className={isMobile ? 'main-container mob' : 'main-container'}>
            <div className="row mob-rev">
                <div className="col-lg-4 thumb-list">
                    {isMobile ?
                        <div className="row videos-row">
                            <div className="col-md-4 col-6">
                                <a onClick={(e) => showContent("thumb","دورة صناعة المحتوى مع هند الناهض","video","")} >
                                    <SmallVideo img="thumb" title="دورة صناعة المحتوى مع هند الناهض" cost="550" download="12" type="video"/>
                                </a>
                            </div>
                            <div className="col-md-4 col-6">
                                <a onClick={(e) => showContent("thumb-2","تعرف على أسرار صناعة المحتوى التسويقي الفعال","video","")} >
                                    <SmallVideo img="thumb-2" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="149" download="39" type="video"/>
                                </a>
                            </div>
                            <div className="col-md-4 col-6">
                                <a onClick={(e) => showContent("","تعرف على أسرار صناعة المحتوى التسويقي الفعال","file","audio")} >
                                    <SmallVideo title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="550" download="12" type="file" file="audio"/>
                                </a>
                            </div>
                            <div className="col-md-4 col-6">
                                <a onClick={(e) => showContent("","فوكس حلقة ٥","file","word")} >
                                    <SmallVideo title="فوكس حلقة ٥" cost="150" download="100" type="file" file="word"/>
                                </a>
                            </div>
                            <div className="col-md-4 col-6">
                                <a onClick={(e) => showContent("","دورة صناعة المحتوى مع هند الناهض","file","pdf")} >
                                    <SmallVideo title="دورة صناعة المحتوى مع هند الناهض" cost="300" download="120" type="file" file="pdf"/>
                                </a>
                            </div>
                        </div>
                    :
                    <div>
                        <div className="row videos-row">
                            <div className="col-4">
                                <a onClick={(e) => showContent("thumb","دورة صناعة المحتوى مع هند الناهض","video","")} >
                                    <SmallVideo img="thumb" title="دورة صناعة المحتوى مع هند الناهض" cost="550" download="12" type="video"/>
                                </a>
                            </div>
                            <div className="col-4">
                                <a onClick={(e) => showContent("thumb-2","تعرف على أسرار صناعة المحتوى التسويقي الفعال","video","")} >
                                    <SmallVideo img="thumb-2" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="149" download="39" type="video"/>
                                </a>
                            </div>
                            <div className="col-4">
                                <a onClick={(e) => showContent("thumb-3","تعرف على أسرار صناعة المحتوى التسويقي الفعال","video","")} >
                                    <SmallVideo img="thumb-3" title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="299" download="104" type="video"/>
                                </a>
                            </div>
                            <div className="col-4">
                                <a onClick={(e) => showContent("","تعرف على أسرار صناعة المحتوى التسويقي الفعال","file","audio")} >
                                    <SmallVideo title="تعرف على أسرار صناعة المحتوى التسويقي الفعال" cost="550" download="12" type="file" file="audio"/>
                                </a>
                            </div>
                            <div className="col-4">
                                <a onClick={(e) => showContent("","فوكس حلقة ٥","file","word")} >
                                    <SmallVideo title="فوكس حلقة ٥" cost="150" download="100" type="file" file="word"/>
                                </a>
                            </div>
                            <div className="col-4">
                                <a onClick={(e) => showContent("","دورة صناعة المحتوى مع هند الناهض","file","pdf")} >
                                    <SmallVideo title="دورة صناعة المحتوى مع هند الناهض" cost="300" download="120" type="file" file="pdf"/>
                                </a>
                            </div>
                        </div>
                    </div>
                    }
                </div>
                <div className="col-lg-8">
                    <VideoBox img={img} title={title} type={type} file={file}/>
                </div>
            </div>
        </div>
    )
}

export default Store;