import React, {useEffect} from 'react';
import { BrowserRouter as Router, Route, Switch, NavLink,Redirect } from 'react-router-dom';
import Home from "./routes/home";
import Store from "./routes/store";
import Prodcast from "./routes/prodcast";
import Courses from "./routes/courses";
import CourseContent from "./routes/courseContent";
import Booked from "./routes/book";
import NavMenu from "./components/navMenu";
import NavMenuMob from "./components/navMenuMob";
import Header from "./components/header";
import HeaderMob from "./components/headerMob";
import HeaderTablet from "./components/headerTablet";
import { useMediaQuery } from 'react-responsive';
import MenuProvider from "react-flexible-sliding-menu";
import Menu from "./components/menu";
import HamMenu from "./components/hamMenu";
import $ from 'jquery';

// css
import './lib/css';

$( document ).ready(function() {
  var vidWidth = $(".video").width();
  $(".video-container").css("height",vidWidth/1.795);
  $(".text-box").css("height",vidWidth/1.795/1.469);
  var courseWidth = $(".course-big-vid").width();
  $(".course-big-vid-container .video-container").css("height",courseWidth/1.795);
  $(".text-box.c").css("height",courseWidth/1.795/1.756);
  $(".mob .text-box.c").css("height",courseWidth/1.795/1.156);
});

$( window ).resize(function() {
  var vidWidth = $(".video").width();
  $(".video-container").css("height",vidWidth/1.795);
  $(".text-box").css("height",vidWidth/1.795/1.469);
  var courseWidth = $(".course-big-vid").width();
  $(".course-big-vid-container .video-container").css("height",courseWidth/1.795);
  $(".text-box.c").css("height",courseWidth/1.795/1.756);
  $(".mob .text-box.c").css("height",courseWidth/1.795/1.156);
});

function App() {
  const isMobile = useMediaQuery({
    query: '(max-device-width: 991px)'
  })
  const isTablet = useMediaQuery({
    query: '(max-device-width: 1350px)'
  })
  const isBigMob = useMediaQuery({
    query: '(min-device-width: 600px)'
  });
  var firstTry = 0;
  const toggleBurgerMenu = (e) => {
    if($(".ham-menu").hasClass("open")){
        $(".ham-menu").removeClass("open");
    }else{
        $(".ham-menu").addClass("open");
    }        
  }
  const addActive = (e) => {
    if(firstTry<5){
      firstTry = firstTry + 1;
    }else{
      if(e){
        var lis = document.getElementsByClassName("liNavMenu");
        [].forEach.call(lis,function(li){
          li.classList.remove("active")
        });
        if(e.url =="/home"){
          document.getElementsByClassName("liNavMenu")[0].classList.add("active")
        }else{
          if(e.url=="/store"){
            document.getElementsByClassName("liNavMenu")[1].classList.add("active")
          }
          else{
            if(e.url=="/prodcast"){
              document.getElementsByClassName("liNavMenu")[2].classList.add("active")
            }else{
              if(e.url=="/course"){
                document.getElementsByClassName("liNavMenu")[3].classList.add("active")
              }else{
                if(e.url=="/book"){
                  document.getElementsByClassName("liNavMenu")[4].classList.add("active")
                }
              }
            }
          }
        }
      }
    }
    
  }
  
  return (
    <MenuProvider width={isMobile?"210px":"346px"} direction={isMobile?"right":"left"} MenuComponent={Menu} animation="push">
      <Router onClick={toggleBurgerMenu}>
        {isMobile ? 
              <div className="fixed-btn">
                <p>
                  حجز عيادة (20 دينار كويتي)
                </p>
              </div> 
            : 
              null
            }
            {
              isMobile?
                <HamMenu/>
                :
                null
            }
        <div className={isMobile ? "app-container" :"desk"}>
          {isMobile ? <NavMenuMob/> : <NavMenu/>}
          <div className={isMobile? "body-bg mob" : "body-bg"}>
            {isMobile ? isBigMob? <HeaderTablet/> : <HeaderMob/> : <Header/>}
            {isMobile ? 
              <div className="dr-row mob">
                <div className="">
                    <img src="../assets/img/dr-img.png" class="dr-img"/>
                </div>
                <div className="dr-info">
                  <p className="dr-title">العيادة الرقمية</p>
                  <p className="dr-name">لـ د. هند الناهض</p>
                  <div className="star-box">
                    <img src="../assets/svg/star.svg" class="star"/>
                    <img src="../assets/svg/star.svg" class="star"/>
                    <img src="../assets/svg/star.svg" class="star"/>
                    <img src="../assets/svg/dark-star.svg" class="star"/>
                    <p className="star-txt">تقييم</p>
                  </div>
                </div>
                <div className="col-4 budget-box">
                  <div className="budget">
                    <p>مشغول بإستشارة</p>
                  </div>
                </div>
              </div>
            : null}
            {isMobile ? 
              <nav className="navMenu mob">
                <div className="menu-overlay"></div>
                <div className="row nav-row">
                  <div className="right-nav-bg">
                    <ul>
                      <li>
                        <NavLink className="navMenuItem" to="/home">نبذه عن هند</NavLink>
                      </li>
                      <li>
                        <NavLink className="navMenuItem" to="/store">متجر</NavLink>
                      </li>
                      <li>
                        <NavLink className="navMenuItem" to="/prodcast">برودكاست</NavLink>
                      </li>
                      <li>
                        <NavLink className="navMenuItem" to="/course">كورسات</NavLink>
                      </li>
                      <li>
                        <NavLink className="navMenuItem" to="/book">حجز عيادة (20 دينار كويتي)</NavLink>
                      </li>
                    </ul>
                  </div>
                </div>
              </nav>
            :
              isTablet?
                <nav className="navMenu tablet">
                  <div className="row nav-row">                 
                    <div className="right-nav-bg">
                      <ul>
                        <li className="liNavMenu active">
                          <NavLink className="navMenuItem" isActive={addActive} to="/home">نبذه عن هند</NavLink>
                          {/* <hr></hr> */}
                        </li>
                        <li className="liNavMenu">
                          <NavLink className="navMenuItem" isActive={addActive}  to="/store">متجر</NavLink>
                          {/* <hr></hr> */}
                        </li>
                        <li className="liNavMenu">
                          <NavLink className="navMenuItem" isActive={addActive}  to="/prodcast">برودكاست</NavLink>
                          {/* <hr></hr> */}
                        </li>
                        <li className="liNavMenu">
                          <NavLink className="navMenuItem" isActive={addActive}  to="/course">كورسات</NavLink>
                          {/* <hr></hr> */}
                        </li>
                        <li className="liNavMenu">
                          <NavLink className="navMenuItem" isActive={addActive}  to="/book">حجز عيادة (20 دينار كويتي)</NavLink>
                          {/* <hr></hr> */}
                        </li>
                        <li className="slider"></li>
                      </ul>
                    </div>
                    <div className="left-nav-bg">
                      <div className="row icons-row">
                        <div className="col-4">
                          <div className="icon-info">
                            <p className="icon-title">سرعة الأنترنت</p>
                            <p className="icon-val">٣٦ Mbps</p>
                          </div>
                          <div className="orange-circle">
                            <img src="../assets/svg/wifi.svg" class="wifi-img"/>
                          </div>
                        </div>
                        <div className="col-4">
                          <div className="icon-info">
                            <p className="icon-title">تجربة الكاميرا </p>
                            <p className="icon-val">لم تقم باختبارها</p>
                          </div>
                          <div className="pink-circle">
                            <img src="../assets/svg/path-1044.svg" class="vid-img"/>
                          </div>
                        </div>
                        <div className="col-4">
                          <div className="icon-info">
                            <p className="icon-title">تجربة الصوت </p>
                            <p className="icon-val">جيد جدآ</p>
                          </div>
                          <div className="green-circle">
                            <img src="../assets/svg/path-1035.svg" class="mic-img"/>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </nav>
              :
                <nav className="navMenu">
                  <div className="row nav-row">                 
                  <div className="col-xl-6 col-lg-8 right-nav-bg">
                    <ul>
                      <li className="liNavMenu active">
                        <NavLink className="navMenuItem" isActive={addActive} to="/home">نبذه عن هند</NavLink>
                        {/* <hr></hr> */}
                      </li>
                      <li className="liNavMenu">
                        <NavLink className="navMenuItem" isActive={addActive}  to="/store">متجر</NavLink>
                        {/* <hr></hr> */}
                      </li>
                      <li className="liNavMenu">
                        <NavLink className="navMenuItem" isActive={addActive}  to="/prodcast">برودكاست</NavLink>
                        {/* <hr></hr> */}
                      </li>
                      <li className="liNavMenu">
                        <NavLink className="navMenuItem" isActive={addActive}  to="/course">كورسات</NavLink>
                        {/* <hr></hr> */}
                      </li>
                      <li className="liNavMenu">
                        <NavLink className="navMenuItem" isActive={addActive}  to="/book">حجز عيادة (20 دينار كويتي)</NavLink>
                        {/* <hr></hr> */}
                      </li>
                      <li className="slider"></li>
                    </ul>
                  </div>
                  <div className="col-xl-6 col-lg-4 left-nav-bg">
                    <div className="row icons-row">
                      <div className="col-4">
                        <div className="icon-info">
                          <p className="icon-title">سرعة الأنترنت</p>
                          <p className="icon-val">٣٦ Mbps</p>
                        </div>
                        <div className="orange-circle">
                          <img src="../assets/svg/wifi.svg" class="wifi-img"/>
                        </div>
                      </div>
                      <div className="col-4">
                        <div className="icon-info">
                          <p className="icon-title">تجربة الكاميرا </p>
                          <p className="icon-val">لم تقم باختبارها</p>
                        </div>
                        <div className="pink-circle">
                          <img src="../assets/svg/path-1044.svg" class="vid-img"/>
                        </div>
                      </div>
                      <div className="col-4">
                        <div className="icon-info">
                          <p className="icon-title">تجربة الصوت </p>
                          <p className="icon-val">جيد جدآ</p>
                        </div>
                        <div className="green-circle">
                          <img src="../assets/svg/path-1035.svg" class="mic-img"/>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                </nav>
            }
            
            <Switch>
              <Redirect exact from="/" to="/home" />
              <Route path="/book">
                <Booked />
              </Route>
              <Route path="/course/:id">
                <CourseContent />
              </Route>
              <Route path="/course">
                <Courses />
              </Route>
              <Route path="/prodcast">
                <Prodcast />
              </Route>
              <Route path="/store">
                <Store />
              </Route>
              <Route path="/home">
                <Home />
              </Route>
            </Switch>
          </div>
        </div>
      </Router>
    </MenuProvider>
 );
}

export default App;
