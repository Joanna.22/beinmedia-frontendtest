import React from 'react';
import {withRouter} from 'react-router-dom';
import $ from 'jquery';

class HeaderMob extends React.Component {
    componentDidUpdate(prevProps) {
        if (this.props.location.pathname !== prevProps.location.pathname) {
            var vidWidth = $(".video").width();
            $(".video-container").css("height",vidWidth/1.795);
            $(".text-box").css("height",vidWidth/1.795/1.469);  
            var courseWidth = $(".course-big-vid").width();
            $(".course-big-vid-container .video-container").css("height",courseWidth/1.795);
            $(".text-box.c").css("height",courseWidth/1.795/1.756);
            $(".mob .text-box.c").css("height",courseWidth/1.795/1.156);
        }
    }
    render() {
        return(
            <div className="whiteBG">
                    <div className="blue-header mob">
                    <div className="row">
                        <div className="col-5 mob-notif">
                            <div className="bell-col">
                                <img src="../assets/svg/white-bell.svg" class="bell"/>
                                <img src="../assets/svg/dot.svg" class="notify"/>
                            </div>
                            <div>
                                <div className="grey-circle">
                                    <img src="../assets/svg/path-994.svg" class="profile-img"/>
                                </div>
                            </div>
                        </div>
                        <div className="col-7 log-box">
                            <img src="../assets/img/Group-71.png" class="logo"/>
                        </div>
                    </div>               
                </div>
                <div className="left-nav-bg mob">
                    <div className="row icons-row">
                        <div className="col-6">
                            <div className="icon-info">
                                <p className="icon-title">سرعة الأنترنت</p>
                                <p className="icon-val">٣٦ Mbps</p>
                            </div>
                            <div className="orange-circle">
                                <img src="../assets/svg/wifi.svg" class="wifi-img"/>
                            </div>
                        </div>
                        <div className="col-3">
                            <div className="pink-circle">
                                <img src="../assets/svg/path-1044.svg" class="vid-img"/>
                            </div>
                        </div>
                        <div className="col-3">
                            <div className="green-circle">
                                <img src="../assets/svg/path-1035.svg" class="mic-img"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default HeaderMob;