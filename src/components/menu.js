import React from 'react';
import { useMediaQuery } from 'react-responsive';

function Menu () {
    const isMobile = useMediaQuery({
        query: '(max-device-width: 991px)'
    })
    return(
        <div className={isMobile? "menu-container mob" : "menu-container"}>
            {isMobile? null : 
                <div>
                    <p className="menu-msg">
                        ١٤ قائمة الأنتظار
                    </p>
                    <hr></hr>
                </div>
            }
            <div className="waiting-list">
                <div className="row waiting-row">
                    <div className="col-5">
                        <div className="light-grey-circle">
                            <img src="../assets/svg/user-light.svg" class="profile-img off"/>
                            <img src="../assets/svg/user-on.svg" class="profile-img on"/>
                        </div>
                    </div>
                    <div className="col-7 waiting-info">
                        <p className="waiting-period">منذ ١٥ دقيقة</p>
                        <p className="waiter-name">مستخدم 1</p>
                    </div>
                </div>
                <div className="row waiting-row">
                    <div className="col-5">
                        <div className="light-grey-circle">
                            <img src="../assets/svg/user-light.svg" class="profile-img off"/>
                            <img src="../assets/svg/user-on.svg" class="profile-img on"/>
                        </div>
                    </div>
                    <div className="col-7 waiting-info">
                        <p className="waiting-period">منذ ١٢ دقيقة</p>
                        <p className="waiter-name">مستخدم 2</p>
                    </div>
                </div>
                <div className="row waiting-row">
                    <div className="col-5">
                        <div className="light-grey-circle">
                            <img src="../assets/svg/user-light.svg" class="profile-img off"/>
                            <img src="../assets/svg/user-on.svg" class="profile-img on"/>
                        </div>
                    </div>
                    <div className="col-7 waiting-info">
                        <p className="waiting-period">منذ ١١ دقيقة</p>
                        <p className="waiter-name">مستخدم 2</p>
                    </div>
                </div>
                <div className="row waiting-row active-user">
                    <div className="col-5">
                        <div className="light-grey-circle">
                            <img src="../assets/svg/user-light.svg" class="profile-img off"/>
                            <img src="../assets/svg/user-on.svg" class="profile-img on"/>
                        </div>
                    </div>
                    <div className="col-7 waiting-info">
                        <p className="waiting-period">منذ ١٠ دقيقة</p>
                        <p className="waiter-name">مالك محمد</p>
                    </div>
                </div>
                <div className="row waiting-row">
                    <div className="col-5">
                        <div className="light-grey-circle">
                            <img src="../assets/svg/user-light.svg" class="profile-img off"/>
                            <img src="../assets/svg/user-on.svg" class="profile-img on"/>
                        </div>
                    </div>
                    <div className="col-7 waiting-info">
                        <p className="waiting-period">منذ ٨ دقيقة</p>
                        <p className="waiter-name">مستخدم 4</p>
                    </div>
                </div>
                <div className="row waiting-row">
                    <div className="col-5">
                        <div className="light-grey-circle">
                            <img src="../assets/svg/user-light.svg" class="profile-img off"/>
                            <img src="../assets/svg/user-on.svg" class="profile-img on"/>
                        </div>
                    </div>
                    <div className="col-7 waiting-info">
                        <p className="waiting-period">منذ ٥ دقيقة</p>
                        <p className="waiter-name">مستخدم 5</p>
                    </div>
                </div>
                <div className="row waiting-row">
                    <div className="col-5">
                        <div className="light-grey-circle">
                            <img src="../assets/svg/user-light.svg" class="profile-img off"/>
                            <img src="../assets/svg/user-on.svg" class="profile-img on"/>
                        </div>
                    </div>
                    <div className="col-7 waiting-info">
                        <p className="waiting-period">منذ ٢ دقيقة</p>
                        <p className="waiter-name">مستخدم 6</p>
                    </div>
                </div>
            </div>
        </div>  
    )
}

export default Menu;