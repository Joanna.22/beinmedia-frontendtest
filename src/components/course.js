import React from 'react';

class Course extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
          img: props.img || "video",
          title: props.title || "",
          cost: props.cost || "",
          num: props.num || "",
        }
    }
    render() {
        return(
            <div className="course-container">
                <div>
                    <img src={"../../assets/img/" + this.props.img + ".jpg"} className="course"/>
                    <div className="course-overlay"></div>
                    <p className="course-vid-num">{this.props.num} فيديو</p>
                    <img src="../../assets/svg/col-vid.svg" className="vid-icon"/>
                </div>
                <div className="row">
                    <div className="col-8 no-left-pad">
                        <p className="course-title">
                            {this.props.title}
                        </p>
                    </div>
                    <div className="col-4 course-cost">
                        <div className="cost-div">
                            <span className="cost-val">{this.props.cost}</span>
                            <p className="thumb-cost"> دينار كويتي </p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Course;