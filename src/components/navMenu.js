import React, { useContext } from 'react';
import { MenuContext } from "react-flexible-sliding-menu";
import $ from 'jquery';


function NavMenu (){
    const { toggleMenu } = useContext(MenuContext);
    const toggleSideMenu = (e) => {
        toggleMenu();
        if($("#root").hasClass("open")){
            $("#root").removeClass("open");
            $("#root").addClass("closeM");
        }else{
            $("#root").addClass("open");
            $("#root").removeClass("closeM");
            $("#root").addClass("desk");
        }        
    }
    return(
        <div className="white-nav">
            <div className="row">
                <div className="bell-col">
                    <img src="../assets/svg/path-995.svg" class="bell"/>
                </div>
                <div className="col-4 ml-auto">
                    <div className="grey-circle">
                        <img src="../assets/svg/path-994.svg" class="profile-img"/>
                    </div>
                    <p className="hello-msg">
                            مرحبآ بك
                    </p>
                    <p className="user-name">مالك محمد</p>
                </div>
                <div className="left-nav">
                    <p className="menu-msg closed" onClick={toggleSideMenu}>
                        <img src="../assets/svg/arrow-right-2.svg" class="arrow-right"/>
                        ١٤ قائمة الأنتظار
                    </p>
                    <p className="menu-msg open" onClick={toggleSideMenu}>
                        <img src="../assets/svg/close-arr.svg" class="arrow-right"/>
                    </p>
                </div>
            </div>
        </div>
    )
}

export default NavMenu;