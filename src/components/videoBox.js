import React from 'react';

class VideoBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          img: props.img || "video",
          title: props.title || "",
          type: props.type || "file",
          file: props.file || "word"
        }
    }
    render() {
        return(
            <div className="video-container">
                {this.props.type == "video"?
                    <div className="course-big-vid">
                        <img src={"../../assets/img/" + this.props.img + ".jpg"} className="video"/>
                        <img src="../../assets/svg/play.svg" className="play"/>
                        <hr className="bar"></hr>
                        <p className="video-time">0:00 / 2:29</p>
                        <img src="../../assets/svg/volume.svg" className="volume"/>
                        <img src="../../assets/svg/full-screen.svg" className="full-screen"/>
                        <div className="vid-overlay"></div>
                    </div>
                :
                    <div className="big-sub-type">
                        <div className="file-view"></div>
                        {
                            this.props.file == "audio"?
                                <p className="file-type word">AUDIO</p>
                            : 
                                this.props.file == "pdf"?
                                    <p className="file-type word">PDF</p>
                                :
                                    <p className="file-type word">WORD</p>
                        }
                        <img src="../../assets/svg/file.svg" className="vid-icon"/>
                        <div className="big-file-overlay"></div>
                    </div>
                }
                <p className="big-vid-title">{this.props.title}</p>
                
            </div>
        )
    }
}

export default VideoBox;