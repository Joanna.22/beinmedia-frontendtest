import React from 'react';
import {withRouter} from 'react-router-dom';
import $ from 'jquery';

class Header extends React.Component {
    componentDidUpdate(prevProps) {
        if (this.props.location.pathname !== prevProps.location.pathname) {
            var vidWidth = $(".video").width();
            $(".video-container").css("height",vidWidth/1.795);
            $(".text-box").css("height",vidWidth/1.795/1.469);  
            var courseWidth = $(".course-big-vid").width();
            $(".course-big-vid-container .video-container").css("height",courseWidth/1.795);
            $(".text-box.c").css("height",courseWidth/1.795/1.756);
        }
    }
    render() {
        return(
            <div className="blue-header">
                <div className="row">
                    <div className="col-6">
                        <div className="dr-row">
                            <div className="col-1">
                                <img src="../assets/svg/icons8-home-1.svg" class="home-img"/>
                            </div>
                            <div className="">
                                <img src="../assets/img/dr-img.png" class="dr-img"/>
                            </div>
                            <div className="dr-info">
                                <p className="dr-title">العيادة الرقمية</p>
                                <p className="dr-name">لـ د. هند الناهض</p>
                                <div className="star-box">
                                    <img src="../assets/svg/star.svg" class="star"/>
                                    <img src="../assets/svg/star.svg" class="star"/>
                                    <img src="../assets/svg/star.svg" class="star"/>
                                    <img src="../assets/svg/dark-star.svg" class="star"/>
                                    <p className="star-txt">تقييم</p>
                                </div>
                            </div>
                            <div className="col-4 budget-box">
                                <div className="budget">
                                    <p>مشغول بإستشارة</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-6 log-box">
                        <img src="../assets/img/Group-71.png" class="logo"/>
                    </div>
                </div>
            </div>
            )
    }
}

export default withRouter(props => <Header {...props}/>);