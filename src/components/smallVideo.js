import React from 'react';

class SmallVideo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          img: props.img || "video",
          title: props.title || "",
          cost: props.cost || "",
          download: props.download || "",
          type: props.type || "file",
          file: props.file || "word"
        }
    }
    render() {
        return(
            <div className="thumb-container">
                {this.props.type == "video" ? 
                    <div>
                        <img src={"../../assets/img/" + this.props.img + ".jpg"} className="thumb"/>
                        <div className="thumb-overlay"></div> 
                        <img src="../../assets/svg/vid.svg" className="vid-icon"/>
                    </div>   
                :
                    <div>
                        <div className="file-bg"></div>
                        <div className="file-overlay"></div>
                        {
                            this.props.file == "audio"?
                                <p className="file-type word">AUDIO</p>
                            : 
                                this.props.file == "pdf"?
                                    <p className="file-type word">PDF</p>
                                :
                                    <p className="file-type word">WORD</p>
                        }
                        <img src="../../assets/svg/file.svg" className="vid-icon"/>
                    </div>
                }
                
                
                <p className="thumb-title">
                    {this.props.title}
                </p>
                <div className="row thumb-row">
                    <div className="col-8 cost-div">
                        <span className="cost-val">{this.props.cost}</span>
                        <p className="thumb-cost"> دينار كويتي </p>
                    </div>
                    <div className="col-4 download-div">
                        <img src="../../assets/svg/download.svg" className="download-icon"/>
                        <p className="thumb-download">{this.props.download}</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default SmallVideo;