import React from 'react';
import {Link} from 'react-router-dom';

function HamMenu () {
    return(
        <div className="ham-menu">
            <ul class="nav navbar-nav">
                <li>
                    <Link to="/home">الرئيسية</Link>
                </li>
                <li>
                    <Link to="/home">نبذه عنا</Link>
                </li>
                <li>
                    <Link to="#">الخبراء</Link>
                </li>
                <li>
                    <Link to="#">سؤال وجواب</Link>
                </li>
                <li>
                    <Link to="#">اختبارات كورونا</Link>
                </li>
                <li>
                    <Link to="#">أتصل بنا</Link>
                </li>
                <li>
                    <Link to="#">أنضم كخبير</Link>
                </li>
            </ul>
        </div>
    )
}

export default HamMenu;