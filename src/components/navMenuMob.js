import React, { useContext } from 'react';
import { MenuContext } from "react-flexible-sliding-menu";
import $ from 'jquery';


function NavMenuMob (){
    const { toggleMenu } = useContext(MenuContext);
    const toggleSideMenu = (e) => {
        toggleMenu();
        if($("#root").hasClass("open")){
            $("#root").removeClass("open");
        }else{
            $("#root").addClass("open");
            $("#root").addClass("mob");
        }        
    };
    const toggleBurgerMenu = (e) => {
        if($(".ham-menu").hasClass("open")){
            $(".ham-menu").removeClass("open");
            $(".fixed-btn").css("visibility","visible");
        }else{
            $(".ham-menu").addClass("open");
            $(".fixed-btn").css("visibility","hidden");
        }        
    }
    return(
        <div className="white-nav mob">
            <div className="left-nav">
                <p className="menu-msg" onClick={toggleSideMenu}>
                    ١٤ قائمة الأنتظار
                </p>
            </div>
            <img src="../../assets/svg/burger.svg" onClick={toggleBurgerMenu} className="burger-menu"/>
        </div>
    )
}

export default NavMenuMob;