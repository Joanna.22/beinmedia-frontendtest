/*======= Include All App Css =======*/

import 'bootstrap/dist/css/bootstrap.min.css';
import '../index.css';
import '../App.css';

// Custom Style File
import '../assets/css/style.css';
import '../assets/css/mob-style.css';
import '../assets/css/responsive.css';